//
//  ViewController.m
//  zigzagPaint
//
//  Created by Duong Tien Quan on 11/5/15.
//  Copyright © 2015 vtvcab. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    printf ("%s\n", "    +       +       +");
//    printf ("%s\n", "   + +     + +     + +");
//    printf ("%s\n", "  +   +   +   +   +   +");
//    printf ("%s\n", " +     + +     + +     +");
//    printf ("%s\n", "+       +       +       +");
//    int i,j;
//    int n = 5;
//    printf("\n");
//    
//    for(i=1;i<=n;i++)
//    {
//        for(j=1;j<=n-i;j++)
//            printf(" ");
//        for(j=1;j<=2*i;j++)
//        {
//            if (j==1||j==2*i-1)
//                printf("+");
//            else
//                printf (" ");
//        }
//        printf("\n");
//        if (i==n) {
//            for(j=1; j<=n;j++)
//                printf (" ");
//            break ;
//        }
//    }
    int n,i,j,d;
    n=5;
    
    int width = (n-1)*8+1;
    
    char vscreen[n][width+1];
    memset(vscreen, ' ', sizeof vscreen);
    for(i=0; i < n; ++i)
        vscreen[i][width] = 0;
    
    for(j=i=0, d=-1; i < width; ++i, j += d){
        vscreen[j][i] = '+';
        if(j == n - 1 || j == 0)
            d = -d;
    }
    printf("\n");
    for(i=n-1;i>=0;--i)
        printf("%s\n", vscreen[i]);
    NSLog(@"%@  %d",@"Quan Duong",32);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
